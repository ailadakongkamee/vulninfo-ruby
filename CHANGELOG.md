## vulninfo-ruby

## [1.3.0] - 2021-06-16
- OWASP mappings (!4)

## [1.2.0] - 2021-09-23
- Ontology update (!2)

## [1.1.0] - 2021-09-19
- Ontology update (!1)

## [1.0.0] - 2021-06-16
- Initial release

