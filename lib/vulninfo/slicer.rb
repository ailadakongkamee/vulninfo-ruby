# frozen_string_literal: true

require_relative 'ontology'

class String
  def clean
    gsub(/[-.():]/, '_').delete(' ')
  end
end

class OntologySlice
  attr_accessor :nodes, :relations, :out_relations, :in_relations

  def initialize
    self.nodes = {}
    self.relations = {}
    self.out_relations = {}
    self.in_relations = {}
  end

  def add_relation(relation, source, dest) 

    src_id = source[:identifier]
    dst_id = dest[:identifier]

    self.nodes[src_id] = source
    self.nodes[dst_id] = dest

    self.out_relations[src_id] = [] unless self.out_relations.key?(src_id)
    self.out_relations[src_id] << relation[:identifier]
    
    self.in_relations[dst_id] = [] unless self.in_relations.key?(dst_id)
    self.in_relations[dst_id] << relation[:identifier]

    self.relations[relation[:identifier]] = relation
  end

  def dot
    s = StringIO.new
    s << "digraph {\n"
    s << "graph [fontname=\"helvetica\"];\n"
    s << "node [fontname=\"helvetica\",shape=\"rectangle\"];\n"
    s << "edge [fontname = \"helvetica\"];\n"

    self.nodes.each do |_k, v|
      s << "n#{v[:identifier].clean}[label=\"#{v[:identifier]}\"];\n"
    end
    self.relations.each do |_k, v|
      s << "n#{v[:sourceid].clean}->n#{v[:targetid].clean}[label=\"#{v[:label]}\"];\n"
    end
    s << '}'
    s.string
  end
end

class NodeResult 
  attr_accessor :level, :node
  def initialize(level, node)
    self.level = level
    self.node = node
  end 
end

class VulnInfoPair
  attr_accessor :first, :second
  def initialize(first, second)
    self.first = first
    self.second = second
  end
end

module Slicer
  module Direction
    FORWARD = 1
    BACKWARDS = 2 
  end

  def self.common_ancestors(firstnode, secondnode, relationKind = RelationKind::PARENT_OF)
    ancestors = []
    a, backseqfirstmap = slice(firstnode, Direction::BACKWARDS, relationKind)
    b, backseqsecondmap = slice(secondnode, Direction::BACKWARDS, relationKind)


    backseqfirstmap.each do |firstid, backseqfirst| 
      backseqsecondmap.each do |secondid, backseqsecond|
        ancestors << VulnInfoPair.new(backseqfirst,backseqsecond) if firstid == secondid
        end
    end

    ancestors.sort! do |a,b| 
      (a.first.level + a.second.level) <=> (b.first.level + b.second.level)
    end

    ancestors
  end

  def self.slice(criterium, direction, relationKind = RelationKind::PARENT_OF)
    ret = OntologySlice.new
    worklist = [NodeResult.new(0, criterium)]
    sequence = {}

    while worklist.size > 0 
      nxt = worklist.pop
      relations = []

      relations += if direction == Slicer::Direction::FORWARD
                    Ontology.out_relations[nxt.node]
      else
                    Ontology.in_relations[nxt.node]
      end
   
      relations.each do |relationid|
        relation = Ontology.relations[relationid]

        next if relation == nil 
        next if relation[:kind] != relationKind

        in_node = Ontology.nodes[relation[:sourceid]]
        out_node = Ontology.nodes[relation[:targetid]]

        ret.add_relation(relation, in_node, out_node)

        nxtnxt = if direction == Slicer::Direction::FORWARD
          NodeResult::new(nxt.level + 1, out_node[:identifier])
        else
          NodeResult::new(nxt.level + 1, in_node[:identifier])
        end

        sequence[nxtnxt.node] = nxtnxt
        worklist.push(nxtnxt)
      end
    end
   
    [ret, sequence]
  end
end
