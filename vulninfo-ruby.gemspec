# frozen_string_literal: true

require_relative "lib/vulninfo/version"

Gem::Specification.new do |spec|
  spec.name          = "vulninfo-ruby"
  spec.version       = Vulninfo::VERSION
  spec.authors       = ["Julian Thome", "Isaac Dawson"]
  spec.email         = ["jthome@gitlab.com", "idawson@gitlab.com"]

  spec.summary       = "The CWE ontology as a ruby gem"
  spec.description   = "This ruby gem exposes the CWE ontology as a ruby gem"
  spec.homepage      = "https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-ruby"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.4.0")

  spec.metadata['allowed_push_host'] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-ruby"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gitlab-org/vulnerability-research/foss/vulninfo/vulninfo-ruby/CHANGELOG.md"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_development_dependency 'solargraph'
  spec.add_development_dependency 'simplecov'
end
