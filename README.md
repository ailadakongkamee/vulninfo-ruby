# vulninfo-ruby

CWE vulnerability information ruby bindings.

This project exposes a partial collection from the 
[Common Weakness Enumeration Dictionary](https://cwe.mitre.org/data/index.html) as a ruby gem.

At the moment we only extract the following relationship types between CWEs:

``` bash
UNKNOWN     
CHILD_OF    
CAN_PRECEDE 
CAN_ALSO_BE 
MAPS_TO     
PARENT_OF   
PEER_OF     
REQUIRES    
STARTS_WITH 
FOLLOWED_BY
```

For CWEs, we extract `identifier`, `title` and `description`.

```
identifier: "cwe-1004",
title: "Sensitive Cookie Without 'HttpOnly' Flag",
description: "The software uses a cookie to store sensitive information, but the cookie is not marked with the HttpOnly flag.",
```

The CWE information is available by means of the `Ontology` module which
provides access to the CWE ontology. As illustrated in the code snippet below,
it provides access to CWEs and their relations (e.g., `child_of`,
`parent_of`, etc.).


``` ruby
module Ontology

  def self.nodes
    @@nods
  end

  def self.relations
    @@rels
  end

  def self.in_relations
    @@in_rels
  end

  def self.out_relations
    @@out_rels
  end
  #...
end
```

In the code snippet below, you can see how you can easily access CWE
information by means of the `Nodes` map that maps CWE identifiers to the
corresponding CWE information. In the code snippet below, we are just printing
out the CWE title and description fields.

``` ruby
node = Ontology.nodes['cwe-78']
puts(node[:description])
puts(node[:title]))
```

Another functionality provided by vulninfo-ruby is the capability of slicing the
CWE ontology. This is useful if you only want to extract a subset of CWEs based
on their relation.

The code snippet below extracts all the nodes that are reachable by `cwe-78` by
transitively following forward edges and which are in a `parent_of`
relationship.

``` ruby
slice, _ = Slicer.slice(cwe22[:identifier], Slicer::Direction::FORWARD)
```

In case you would like to compute whether two given CWEs have a common ancestor
and what the distance to the ancestor is, you can use the code snippet below:

``` ruby
ancestors = Slicer.common_ancestors('cwe-39', 'cwe-32')
```

This function searches for the common ancestors for both `cwe-39` and `cwe-32`
and returns a sorted array of ancestor (sorted by distance from closed to
farthest). 

## License 

The data in this repository is derived from MITRE Common Weakness Enumerations
(CWE).  The MITRE Corporation grants non-exclusive, royalty-free license to use
CWE for research, development, and commercial purposes. For more information,
please have a look at Terms.md or https://cwe.mitre.org/about/termsofuse.html.

The code itself is licensed used under the [MIT license](LICENSE.txt).
